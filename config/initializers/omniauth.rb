Rails.application.config.middleware.use OmniAuth::Builder do
  provider :steam, Rails.application.config.STEAM_API_KEY
end
OmniAuth.config.on_failure = WelcomeController.action(:auth_callback)