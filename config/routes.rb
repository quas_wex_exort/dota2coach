Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'
  post 'auth/steam/callback' => 'welcome#auth_callback'
  get 'login' => 'welcome#login'
  delete 'logout', :controller => 'welcome', :action => 'logout'
  get 'account' => "accounts#show"
  get 'conversations' => "conversations#show"

  post '/profiles/:id', to: 'profiles#show'

  get '/payment/success', to: 'payment#success'
  match '/payment/failure'  => 'payment#failure', via: [:get, :post]
  get '/payment/pay', to: 'payment#pay'

  resources :profiles, only: [:show,:edit,:update] do
		member do
			get 'populate_coaches'
			get 'new_add_coach'
			put 'add_coach'
			put 'send_friend_request'
			get 'populate_reviews'
      put 'verify_mmr'
      put 'verify'
      put 'un_verify'
		end
	end

	resources :friends, only: [:index] do
		member do
			put 'send_request'
			put 'cancel_request'
			put 'decline_request'
			get 'requests'
			put 'accept_request'
		end
	end

	resources :coaches, only: [:index, :new, :create] do
		member do
			put 'free_request'
			put 'accept_free_request'
			put 'cancel_free_request'
			put 'decline_free_request'
			get 'review_rating'
			get 'sign_in'
			put 'add_review_rating'
		end
	end

	resources :notifications, only: [:show] do
		member do
			get 'friend_requests'
			get 'free_coaching_requests'
			get 'coaching_requests'
		end
	end

  resources :conversations
  # do
  resources :messages
  resources :search
  resources :contact
  # end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

end
