# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# todo: create peru and other latest regions
	Regions.create(names: ["Europe East", "Europe West", "US East", "US West", "South America", "Russia", "SE Asia", "Australia", "India"])
	Languages.create(names: ['Arabic', 'Bengali', 'Chinese', 'English', 'French', 'German', 'Hindi', 'Italian', 'Japanese', 'Javenese', 'Korean', 'Malay - Indonesian', 'Mandarin', 'Polish', 'Portuguese', 'Russian', 'Spanish', 'Swedish', 'Thai', 'Turkish', 'Urdu', 'Vietnamese'])
