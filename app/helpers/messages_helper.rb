module MessagesHelper
  def self_or_other(message)
    current_user = User.find_by(steam_id: session['current_user']['uid'])
    message.from_user == current_user ? 'self' : 'other'
  end

  def set_new_messager_ids
    if session[:current_user]
      current_user = User.find_by(steam_id: session['current_user']['uid'])
      @new_messager_ids = []
      current_user.latest_messages.each { |message|
        if message.to_user==current_user and !message.is_read
          @new_messager_ids.push message.from_user.steam_id
        end
      }
    end
  end

  def get_new_message_count
    @new_messager_ids.length > 0 ? '('+ @new_messager_ids.length.to_s + ')' : ''
  end

  def get_friends()
    current_user = User.find_by(steam_id: session['current_user']['uid'])
    friends = []
    current_user.friends.each { |friend|
      friends.push({'name' => friend.steam_nickname, 'steam_id' => friend.steam_id})
    }
    return friends
  end
end
