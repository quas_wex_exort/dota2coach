class MessageTo
  include Neo4j::ActiveRel

  from_class Message
  to_class   User
  type 'message_to'

end