class PreviousNotification
  include Neo4j::ActiveRel

  from_class CoachNotification
  to_class   CoachNotification
  type 'previous_notification'

end
