class CoachedTo
  include Neo4j::ActiveRel

  from_class CoachData
  to_class User

  type 'coached_to'

end
