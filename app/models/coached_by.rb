class CoachedBy
  include Neo4j::ActiveRel

  to_class User
  from_class CoachData

  type 'coached_by'

end
