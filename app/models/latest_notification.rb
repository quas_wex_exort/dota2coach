class LatestNotification
  include Neo4j::ActiveRel

  from_class User
  to_class   :CoachNotification
  type 'latest_notification'

end
