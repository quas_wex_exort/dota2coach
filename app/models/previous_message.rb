class PreviousMessage
  include Neo4j::ActiveRel

  from_class Message
  to_class   Message
  type 'previous_message'

end