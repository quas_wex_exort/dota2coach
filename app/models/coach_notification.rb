class CoachNotification
    include Neo4j::ActiveNode
    property :body
    property :is_read, type: Boolean, default: false
    property :timestamp

    has_one :out, :previous_notification, model_class: :CoachNotification, rel_class: PreviousNotification

    has_one :in, :from_user, model_class: :User, rel_class: NotifiedBy
    has_one :in, :latest_for, model_class: :User, rel_class: LatestNotification

    #below rel is for ease of quering
    # has_many :both, :users, model_class: :User, rel_class: MessageTo

end