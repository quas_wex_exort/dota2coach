class CoachingSession
  	include Neo4j::ActiveNode

  	property :payment_id
  	property :payment_gateway, type: String
  	property :total_fees, type: Float
	property :hours, type: Integer
	property :rate, type: Float
	property :review, type: String
	property :rating, type: String
  	property :since, type: Integer
end
