class RequestedFriendshipOf
  include Neo4j::ActiveRel

  from_class User
  to_class   User
  type 'requested_friendship_of'

  property :since, type: Integer
  property :is_read, type: Boolean, default: false

  validates_presence_of :since

end
