class MessageFrom
  include Neo4j::ActiveRel

  from_class User
  to_class   Message
  type 'message_from'

end