class LatestMessage
  include Neo4j::ActiveRel

  from_class User
  to_class   Message
  type 'latest_message'

end