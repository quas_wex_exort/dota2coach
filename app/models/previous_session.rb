class PreviousSession
  include Neo4j::ActiveRel

  from_class CoachingSession
  to_class CoachingSession

  type 'previous_session'

end
