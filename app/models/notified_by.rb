class NotifiedBy
  include Neo4j::ActiveRel

  from_class User
  to_class   CoachNotification
  type 'notified_by'

end
