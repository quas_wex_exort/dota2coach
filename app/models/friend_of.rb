class FriendOf
  include Neo4j::ActiveRel

  from_class User
  to_class   User
  type 'friend_of'

  property :since, type: Integer
  property :blocked, type: Boolean

  validates_presence_of :since

end
