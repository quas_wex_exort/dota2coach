class LatestSession
  include Neo4j::ActiveRel

  from_class CoachData
  to_class CoachingSession

  type 'latest_session'
end
