class User
  	include Neo4j::ActiveNode
  	property :steam_id, index: :exact, constraint: :unique
  	property :steam_nickname
  	property :steam_image
  	property :mmr
  	property :mmr_verified
  	property :regions
  	property :is_a_coach
  	property :languages
  	property :dota_buff_link
  	property :about_me
  	property :coaching_rate
  	property :provides_free_coahing
	property :average_rating, type: Float, default: 0
	property :total_rating_count, type: Integer, default: 0
	property :total_review_count, type: Integer, default: 0
	property :best_review
	property :best_rating, type: Integer, default: 3
	property :worst_review
	property :worst_rating, type: Integer, default: 3
        property :is_verified, type: Boolean, default: false
        property :is_admin, type: Boolean, default: false
        property :mmr_verified, type: Boolean, default: false

  	has_many :both, :friends, model_class: :User, rel_class: FriendOf
  	has_many :out, :latest_messages, model_class: :Message, rel_class: LatestMessage
  	has_one :out, :latest_notification, model_class: :CoachNotification, rel_class: LatestNotification
  	has_many :out, :notifications, model_class: :CoachNotification, rel_class: NotifiedBy
  	has_many :in, :coach_of_data, model_class: :CoachData, rel_class: CoachedBy
  	has_many :in, :coached_by_data, model_class: :CoachData, rel_class: CoachedTo
  	has_many :in, :friend_request, model_class: :User, rel_class: RequestedFriendshipOf
  	has_many :in, :coaching_request, model_class: :User, rel_class: RequestedFreeCoachingFrom

  	validates :steam_id, :presence => true
  	validates :mmr, numericality: { only_integer: true , :greater_than_or_equal_to => 0}
  	validates :coaching_rate, numericality: { only_integer: true, :greater_than_or_equal_to => 0 }
  	# todo: not sure if above validations are really working, wiki says it should but i dont see it happening !!

	def has_both_review
		total_review_count > 1 && !best_review.blank? && !worst_review.blank?	
	end

  	def same_as(current_user)
		return steam_id == current_user['uid'] unless current_user.blank?
		return false
  	end

  def can_be_verified_by(admin_id)
    admin = User.find_by(steam_id: admin_id)
    return admin.is_admin
  end

  	def friend_of(current_user)
	  	return false if current_user.blank? || same_as(current_user)
	  	current_user_node = User.find_by(steam_id: current_user["uid"]) 
	  	return friends.include?(current_user_node)
  	end

  	def pending_friend_request(current_user)
	  	return false if current_user.blank? || same_as(current_user)
	  	current_user_node = User.find_by(steam_id: current_user["uid"]) 
	  	return friend_request.include?(current_user_node)
  	end

  	def pending_free_coaching_request(current_user)
	  	return false if current_user.blank? || same_as(current_user)
	  	current_user_node = User.find_by(steam_id: current_user["uid"]) 
	  	return coaching_request.include?(current_user_node)
  	end

end
