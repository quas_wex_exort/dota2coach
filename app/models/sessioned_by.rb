class SessionedBy
  include Neo4j::ActiveRel

  from_class CoachData
  to_class   CoachingSession
  type 'sessioned_by'

end
