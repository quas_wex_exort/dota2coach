class Message
  include Neo4j::ActiveNode
  property :body
  property :is_read, type: Boolean
  property :timestamp, type: Integer

  has_one :out, :previous_message, model_class: :Message, rel_class: PreviousMessage

  has_one :in, :from_user, model_class: :User, rel_class: MessageFrom
  has_one :out, :to_user, model_class: :User, rel_class: MessageTo

  has_one :in, :latest_for, model_class: :User, rel_class: LatestMessage

  #below rel is for ease of quering
  # has_many :both, :users, model_class: :User, rel_class: MessageTo

end