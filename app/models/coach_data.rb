class CoachData
  	include Neo4j::ActiveNode

  	property :last_session, type: Integer
	property :coach_id, type: Integer
	property :coachee_id, type: Integer

	has_one :out, :latest_session, model_class: :CoachingSession, rel_class: LatestSession
	has_one :out, :coached_by, model_class: :User, rel_class: CoachedBy
	has_one :out, :coached_to, model_class: :User, rel_class: CoachedTo
	has_many :out, :sessions, model_class: :CoachingSession, rel_class: SessionedBy 

end
