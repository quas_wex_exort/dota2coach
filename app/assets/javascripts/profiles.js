var enable_coaching_rate = function(){
	var is_a_coach = $('#user_is_a_coach_true').is(":checked");
	if(is_a_coach)
		show_coaching_details();
	else
		hide_coahing_details();
	$("#user_is_a_coach_true").on("click", function(){
		show_coaching_details();
	});
	$("#user_is_a_coach_false").on("click", function(){
		hide_coahing_details();
	});
};
var hide_coahing_details = function(){
	var coaching_rate = $("#coaching_rate");
	var free_coaching = $("#free_coaching");
	coaching_rate.hide();
	free_coaching.hide();
};

var show_coaching_details = function(){
	var coaching_rate = $("#coaching_rate");
	var free_coaching = $("#free_coaching");
	coaching_rate.show();
	free_coaching.show();
};
