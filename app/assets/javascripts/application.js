// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require private_pub
//= require_tree .
//= require bootstrap/modal
var d2c = d2c || {};

var populateModalWindow = function(title,html){
	modal_window = $('#modal_window');
	initialize(modal_window,title,html);
	modal_window.modal();
	modal_window.show();
};

var initialize = function(modal,title,html){
	modal_body = modal.find('.modal-body');
	modal_header = modal.find('.modal-header .modal-title');
	modal_body.html(html);
	modal_header.html(title);
};

var populateFlashMessage = function(message, message_type){
	var flash = document.createElement("div");
	if(message_type=="notice")
		flash.setAttribute('class', 'alert alert-info flash_orientation');
	flash.appendChild(document.createTextNode(message));
	$('.flash_message').html('');
	$('.flash_message').append(flash);
};

var highlightActiveMenu = function(){
	$("ul.nav li").click(function(){
		$("ul.nav li").removeClass("active");
		$(this).addClass("active");
	});
};

$(function(){
	$("#chat-button").click(function(){
		$(".chat-section").toggle('slow');
	});

    highlightActiveMenu();

    $(".faq-link").click(function(){
    	$(".faq-overlay").css('display','block');
    });
    $("#faq-close").on('click',function(){
    	$(".faq-overlay").css('display','none');
    });
});

    

