var d2c = d2c || {};

d2c.startConversation = function () {
    var sender_id = $(this).data('sid');
    var recipient_id = $(this).data('rip');

    var conversation_id = sender_id < recipient_id ? sender_id + recipient_id : recipient_id + sender_id;
    //lower + higher string
    chatBox.chatWith(conversation_id, recipient_id);
};

var ready = function () {

    /**
     * When the send message link on our home page is clicked
     * send an ajax request to our rails app with the sender_id and
     * recipient_id
     */

    $('.start-conversation').off('click', d2c.startConversation);
    $('.start-conversation').click(d2c.startConversation);

    /**
     * Used to minimize the chatbox
     */

    $(document).on('click', '.toggleChatBox', function (e) {
        e.preventDefault();

        var id = $(this).data('cid');
        chatBox.toggleChatBoxGrowth(id);
    });

    /**
     * Used to close the chatbox
     */

    $(document).on('click', '.closeChat', function (e) {
        e.preventDefault();

        var id = $(this).data('cid');
        chatBox.close(id);
    });


    /**
     * Listen on keypress' in our chat textarea and call the
     * chatInputKey in chat.js for inspection
     */

    $(document).on('keydown', '.chatboxtextarea', function (event) {

        var id = $(this).data('cid');
        chatBox.checkInputKey(event, $(this), id);
    });

    /**
     * When a conversation link is clicked show up the respective
     * conversation chatbox
     */

    $('a.conversation').click(function (e) {
        e.preventDefault();

        var conversation_id = $(this).data('cid');
        chatBox.chatWith(conversation_id);
    });

    window.addEventListener("popstate", function (e) {
        $.ajax({
            url: e.state,
            dataType: 'script',
            accepts: {
                text: "application/javascript"
            }
        });
    });

    $('a[data-remote = true]').click(function () {
        if($(this).hasClass("push-state"))
            history.pushState($(this).attr('href'), "Dota2Connect", $(this).attr('href'));
    })

};

$(document).ready(ready);
$(document).on("page:load", ready);
$(document).ajaxComplete(function () {
    try {
        FB.XFBML.parse();
    } catch (ex) { }
});
