class NotificationsController < ApplicationController
	before_action :populate_user
	before_action :populate_notification, only: [:coaching_requests]
	before_action :populate_friend_requests, only: [:friend_requests, :show]
	before_action :populate_friend_request_count, only: [:friend_requests, :show]
	before_action :populate_free_coaching_requests, only: [ :show, :coaching_requests]
	before_action :populate_free_coaching_request_count, only: [ :show, :coaching_requests]
	before_action :friend_request_seen, only: [:show, :friend_requests]
	before_action :coaching_notification_seen, only: [:coaching_requests]

	def friend_requests
	end

	def show
	end

	def coaching_requests
	end

	private
	def populate_user
		@user = User.find_by(steam_id: params[:id])
		@user.languages = [] if @user.languages.nil?
		@user.regions = [] if @user.regions.nil?
	end

	def populate_friend_requests
		@friend_requests = @user.friend_request
	end

	def populate_friend_request_count
		friend_requests_count = @user.friend_request_rels.count{|rel| rel.is_read == false}
		@formatted_friend_request_count=""
		@formatted_friend_request_count="(#{friend_requests_count})" if friend_requests_count > 0
	end

	def friend_request_seen
		@user.friend_request_rels.each {|req| req.update_attribute("is_read", true)}
	end

	def coaching_notification_seen
		@user.notifications.each {|n| n.update_attribute("is_read", true)}
		@user.coaching_request_rels.each {|req| req.update_attribute("is_read", true)}
	end

	def populate_free_coaching_requests
		@coaching_requests = @user.coaching_request
	end

	def populate_notification
		@notifications = @user.notifications.sort{|n| -n[:timestamp]}
	end

	def populate_free_coaching_request_count
		coaching_request_count = @user.coaching_request_rels.count{|rel| rel.is_read == false}
		notification_count = @user.notifications.find_all{|n| n.is_read == false}.count
		@formatted_coaching_request_count=""
		@formatted_coaching_request_count="(#{coaching_request_count+notification_count})" if coaching_request_count+notification_count > 0
	end

end
