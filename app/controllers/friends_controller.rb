class FriendsController < ApplicationController
	before_action :populate_user 
	before_action :populate_friend_requests, only: [:requests, :decline_request, :cancel_request, :accept_request]
	def index
		
	end

	def send_request
		@friend = User.find_by(steam_id: params[:friend_id])
    @friend_id = params[:friend_id]
		RequestedFriendshipOf.new(from_node: @user, to_node: @friend, since: Time.now.to_i).save
	end

	def cancel_request
		@friend = User.find_by(steam_id: params[:friend_id])
		@friend.friend_request.destroy(@user)
	end

	def decline_request
    @friend_id = params[:friend_id]
		@friend_request = User.find_by(steam_id: params[:friend_id])
		@user.friend_request.destroy(@friend_request)
	end

	def accept_request
    @friend_id = params[:friend_id]
		@friend_request = User.find_by(steam_id: params[:friend_id])
		@user.friend_request.destroy(@friend_request)
		FriendOf.new(from_node: @user, to_node: @friend_request, since: Time.now.to_i).save
	end

	private
	def populate_user
		@user = User.find_by(steam_id: params[:id])
		@user.languages = [] if @user.languages.nil?
		@user.regions = [] if @user.regions.nil?
	end

	def populate_friend_requests
		@friend_requests = @user.friend_request
	end
end
