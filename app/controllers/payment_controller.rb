include PayPal::SDK::REST
class PaymentController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => :failure

  def success
  end

  def failure
  end

  def pay
    hours = params[:hours].to_i < 1 ? 1 : params[:hours].to_i
    return_url = request.protocol +  request.host_with_port + coaches_path(:coach => params[:coach_id], :hours => hours)
    coach = User.find_by(steam_id: params[:coach_id])

    @payment = PayPal::SDK::REST::Payment.new({
                                                  :intent => "sale",
                                                  :payer => {
                                                      :payment_method => "paypal" },
                                                  :redirect_urls => {
                                                      :return_url => return_url,
                                                      :cancel_url => request.protocol +  request.host_with_port + payment_failure_path },
                                                  :transactions => [{
                                                                        :item_list => {
                                                                            :items => [{
                                                                                           :name => "Coaching Session",
                                                                                           :sku => "Coaching Session",
                                                                                           :price => (coach.coaching_rate.to_i * hours.to_i).to_s,
                                                                                           :currency => "USD",
                                                                                           :quantity => 1 }]},
                                                                        :amount => {
                                                                            :total => (coach.coaching_rate.to_i * hours.to_i).to_s,
                                                                            :currency => "USD" },
                                                                        :description => "Charging for the Coaching Session" }]

                                              } )

    if @payment.create
      approval_url =  @payment.links.select { |link| link.rel == 'approval_url'}[0].href
      session['payment_id'] = @payment.id
      redirect_to approval_url
    else
      redirect_to payment_failure_path
    end

  end

end