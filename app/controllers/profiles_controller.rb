class ProfilesController < ApplicationController
  before_action :populate_user
  skip_before_filter :verify_authenticity_token, :only => :show

  before_filter :set_cache_buster

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def show
  end

  def edit
  end

  def populate_profile
  end

  def populate_coaches
    @coach_data = @user.coached_by_data
  end

  def populate_reviews
    @coach_data = @user.coach_of_data
  end

  def verify_mmr
    @user.update(mmr_verified: true)
  end

  def verify
    @user.update(is_verified: true)
  end

  def un_verify
    @user.update(is_verified: false)
  end

  def update
    user_attributes = user_params
    user_attributes['mmr']=user_attributes['mmr'].to_i
    user_attributes['coaching_rate']=user_attributes['coaching_rate'].to_i
    user_attributes['is_a_coach']=user_attributes['is_a_coach'] == 'true'
    user_attributes['provides_free_coahing'] = user_attributes['provides_free_coahing'] == "true"
    if @user.update(user_attributes)
      render 'populate_profile.js'
    else
      render 'edit.js'
    end
  end

  private
  def populate_user
    @user = User.find_by(steam_id: params[:id])
    @user.languages = [] if @user.languages.nil?
    @user.regions = [] if @user.regions.nil?
  end

  def user_params
    params.require(:user).permit(:mmr, :is_a_coach, :dota_buff_link,:about_me,:coaching_rate, :provides_free_coahing, regions: [], languages: [])
  end

end
