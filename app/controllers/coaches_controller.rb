require 'money'
require 'money/bank/google_currency'

class CoachesController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => :create
  before_action :populate_coachee, only: [:free_request, :cancel_free_request, :decline_free_request, :accept_free_request]
  before_action :populate_coach, only: [:new,:free_request,:cancel_free_request, :decline_free_request, :accept_free_request, :review_rating]
  before_action :populate_notification, only: [:accept_free_request, :decline_free_request]


  def new
    @coachee = User.find_by(steam_id: params[:id])
    @hours=[1,2]
    @coaching = CoachingSession.new(since: Time.now.to_i, rate: @coach.coaching_rate)
    @CCAVENUE_MERCHANT_ID = Rails.application.config.CCAVENUE_MERCHANT_ID
    encryption_key = Rails.application.config.CCAVENUE_ENCRYPTION_KEY
    @CCAVENUE_URL = Rails.application.config.CCAVENUE_URL
    @CCAVENUE_ACCESS_CODE = Rails.application.config.CCAVENUE_ACCESS_CODE

    return_url_1 = {redirect_url: request.protocol +  request.host_with_port + coaches_path(:coach => params[:coach_id], :hours => "1")}.to_query
    return_url_2 = {redirect_url: request.protocol +  request.host_with_port + coaches_path(:coach => params[:coach_id], :hours => "2")}.to_query
    cancel_url = request.protocol + request.host_with_port + payment_failure_path

    Money::Bank::GoogleCurrency.ttl_in_seconds = 86400

    Money.default_bank = Money::Bank::GoogleCurrency.new
    amount1 = (Money.new(100, 'USD').exchange_to('INR').cents/100.0).to_s
    amount2 = (Money.new(200, 'USD').exchange_to('INR').cents/100.0).to_s
    crypto = Crypto.new
    # using merchant_param1 as the number of hours
    raw_request_1 = "merchant_id=71199&amount="+amount1.to_s+"&merchant_param1=1&merchant_param2="+params[:coach_id]+"&order_id="+Time.now.to_i.to_s+"&currency=INR&"+return_url_1+"&cancel_url="+cancel_url+"&language=EN"
    @encrypted_request = crypto.encrypt(raw_request_1, encryption_key)
    raw_request_2 = "merchant_id=71199&amount="+amount2.to_s+"&merchant_param1=2&merchant_param2="+params[:coach_id]+"&order_id="+Time.now.to_i.to_s+"&currency=INR&"+return_url_2+"&cancel_url="+cancel_url+"&language=EN"
    @encrypted_request2 = crypto.encrypt(raw_request_2, encryption_key)

  end

  def index
    # only for paypal
    payment_id = session['payment_id']
    session.delete(:payment_id)
    payment = Payment.find(payment_id)
    if payment.execute( :payer_id => params[:PayerID] )
      coach = User.find_by(steam_id: params[:coach])
      coachee = User.find_by(steam_id: session[:current_user]['uid'])
      # todo: wtf is this hours error handling ? isn't this a call back from paypal?
      hours = params[:hours]
      if hours.blank?
        @errors = "Please select an hour"
        return
      end
      amount_paid = hours.to_i * coach.coaching_rate.to_i
      create_notification(coach,coachee.steam_nickname+" has paid an amount of $"+amount_paid.to_s)
      if create_new_session_connection(coach,coachee, hours, amount_paid, payment_id, 'paypal')
        redirect_to profile_path(:id => params[:coach]), notice: 'Payment Successful, you may now speak to your coach and begin the coaching session' , :flash => { :friend_id => params[:coach]}
      else
        redirect_to profile_path(:id => params[:coach]), notice: 'Payment already processed for this transaction, you may speak to your coach and begin the coaching session'
      end
    else
      redirect_to payment_failure_path
    end
  end

  def create
    # only for ccavenue
    workingKey=Rails.application.config.CCAVENUE_ENCRYPTION_KEY
    encResponse=params[:encResp]
    crypto = Crypto.new
    decResp=crypto.decrypt(encResponse,workingKey)
    decResp = decResp.split("&")
    orderStatus = get_response_parameter(decResp, 'order_status')
    if orderStatus == 'Success'
      coach = User.find_by(steam_id: get_response_parameter(decResp, 'merchant_param2'))
      coachee = User.find_by(steam_id: session[:current_user]['uid'])
      hours = get_response_parameter(decResp, 'merchant_param1')
      amount_paid = hours.to_i * coach.coaching_rate.to_i
      create_notification(coach,coachee.steam_nickname+' has paid an amount of $'+amount_paid.to_s)
      if create_new_session_connection(coach,coachee, hours, amount_paid, get_response_parameter(decResp, 'tracking_id'), 'ccavenue')
        redirect_to profile_path(:id => params[:coach]), notice: 'Payment Successful, you may now speak to your coach and begin the coaching session' , :flash => { :friend_id => params[:coach]}
      else
        redirect_to profile_path(:id => params[:coach]), notice: 'Payment already processed for this transaction, you may speak to your coach and begin the coaching session'
      end
    else
      redirect_to payment_failure_path
    end
  end

  def get_response_parameter(decResp, key)
    (decResp.select { |reponseParameter| reponseParameter.start_with?(key+ '=') })[0].sub(key+'=', '')
  end

  def free_request
    @friend_id = params[:coach_id]
    RequestedFreeCoachingFrom.new(from_node: @coachee, to_node: @coach, since: Time.now.to_i).save
  end

  def cancel_free_request
    @coach.coaching_request.destroy(@coachee)
  end

  def decline_free_request
    @friend_id = params[:coachee_id]
    @coach.coaching_request.destroy(@coachee)
    @coaching_requests = @coach.coaching_request
    create_notification(@coachee, @coach.steam_nickname+" has declined your free coching request")
  end

  def accept_free_request
    @friend_id = params[:coachee_id]
    @coach.coaching_request.destroy(@coachee)
    create_new_session_connection(@coach, @coachee, "1", 0, nil, nil)
    @coaching_requests = @coach.coaching_request
    create_notification(@coachee, @coach.steam_nickname+" has accepted your free coching request")
  end

  def review_rating
    @session = CoachingSession.find_by(uuid: params[:uuid])
    @coach = User.find_by(steam_id: params[:id])
    @review_ratings = ["1","2","3","4","5"]
  end

  def add_review_rating
    @coach = User.find_by(steam_id: params[:id])
    @session = CoachingSession.find_by(uuid: params[:uuid])
    save_average_rating
    update_profile_review
    @coach.save!
    @session.update(session_params)	
  end

  private
  def update_profile_review
    return if session_params[:review].blank? || session_params[:rating].blank?
    @coach.total_review_count+=1 if @session.review.blank?
    current_rating = session_params[:rating].to_i
    if current_rating >= @coach.best_rating
      @coach.best_rating = current_rating
      @coach.best_review = session_params[:review]
    elsif current_rating <= @coach.worst_rating
      @coach.worst_rating = current_rating
      @coach.worst_review = session_params[:review]
    end
  end

  def save_average_rating
    return if session_params[:rating].blank? 
    rating_count = @coach.total_rating_count
    rating_sum = @coach.average_rating * rating_count 
    rating_count+= 1 if @session.rating.blank?
    rating_sum-= @session.rating.to_i unless @session.rating.blank?
    rating_sum+= session_params[:rating].to_i
    @coach.average_rating = rating_sum / rating_count
    @coach.total_rating_count = rating_count
  end

  def create_new_session_connection(coach, coachee, hours, amount_paid, payment_id, payment_gateway)
    new_coaching_data = false
    coach_data = CoachData.find_by(coach_id: coach.steam_id.to_i, coachee_id: coachee.steam_id.to_i)
    if coach_data.blank?
      new_coaching_data = true
      coach_data = CoachData.new(last_session: Time.now.to_i, coach_id: coach.steam_id.to_i, coachee_id: coachee.steam_id.to_i) 
      coach_from = CoachedBy.new(to_node: coach, from_node: coach_data)
      coach_to = CoachedTo.new(from_node: coach_data , to_node: coachee)
    end
    current_session = CoachingSession.new(since: Time.now.to_i, rate: coach.coaching_rate, hours: hours, total_fees: amount_paid, payment_id: payment_id, payment_gateway: payment_gateway)

    # gyan: this helps against replay attacks
    if !coach_data.latest_session.blank? and (coach_data.latest_session.payment_id == payment_id and payment_id != nil)
      return false
    end

    previous_session = coach_data.latest_session
    previous_session_link = PreviousSession.new(from_node: current_session, to_node: previous_session) unless previous_session.blank?
    if new_coaching_data
      coach_data.save
      coach_from.save
      coach_to.save
    end
    current_session.save
    coach_data.latest_session = current_session
    previous_session_link.save unless previous_session_link.blank?
    SessionedBy.new(from_node: coach_data, to_node: current_session).save
  end

  def coaching_params
    params.require(:coach_of).permit(:hours, :coach, :coachee)
  end

  def session_params
    params.require(:coaching_session).permit(:rating,:review)
  end

  def populate_coach
    @coach = User.find_by(steam_id: params[:coach_id])
  end

  def populate_coachee
    @coachee = User.find_by(steam_id: params[:coachee_id])
  end

  def populate_notification
    @notifications = @coach.notifications.sort{|n| -n[:timestamp]}
  end

  def create_notification(user,notification_msg)
    latest_notification = CoachNotification.new(body: notification_msg, timestamp: Time.now.to_i, is_read: false, from_user: user)
    previous_notification = user.latest_notification
    previous_notification_link = PreviousNotification.new(from_node: latest_notification, to_node: previous_notification) unless previous_notification.blank?
    latest_notification.save
    user.latest_notification = latest_notification
    previous_notification_link.save unless previous_notification.blank?
  end

end
