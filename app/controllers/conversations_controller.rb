class ConversationsController < ApplicationController

  layout false

  def create
    conversation_id = (session[:current_user]['uid'].to_i > params[:id].to_i) ? (params[:id] + session[:current_user]['uid']) :
        (session[:current_user]['uid'] + params[:id])
    # Create unique conversation id, lower + higher
    render json: {conversation_id: conversation_id}
  end

  def show

    conversation_id = (session[:current_user]['uid'] > params[:id]) ? (params[:id] + session[:current_user]['uid']) :
        (session[:current_user]['uid'] + params[:id])
    @conversation_id = conversation_id
    @recepient_id = params[:id]

    current_user = User.find_by(steam_id: session['current_user']['uid'])
    friend_user = User.find_by(steam_id: @recepient_id)

    latest_message = current_user.latest_messages.detect { |message|
      message.from_user == friend_user or message.to_user == friend_user
    }

    if latest_message != nil and latest_message.to_user == current_user
      latest_message.is_read = true
      latest_message.save
    end

    @messages = []
    if latest_message != nil
      index = 0
      @messages.push(latest_message)
      while latest_message.previous_message != nil and index < Rails.application.config.MAX_MESSAGE_LENGTH
        @messages.push(latest_message.previous_message)
        latest_message = latest_message.previous_message
        index+=1
      end
    end
    @messages.reverse!

    @reciever_id = params[:id]
    @reciever = User.find_by(steam_id: params[:id]).steam_nickname

  end

end
