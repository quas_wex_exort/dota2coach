class WelcomeController < ApplicationController
  # auth callback POST comes from Steam so we can't attach CSRF token
  skip_before_filter :verify_authenticity_token, :only => :auth_callback
  # todo: this poses a potential security threat ...will need to work around this when we're BIG
  #todo: rename to sessions controller and move out index method to a different controller, this place should only have login and logout

  def index
    @regions = Regions.all.map { |r| r.names }[0]
    if session.key? :current_user
      current_user = User.find_by(steam_id: session['current_user']['uid'])
      @friends = []
      current_user.friends.each { |friend|
        @friends.push({'name' => friend.steam_nickname, 'steam_id' => friend.steam_id})
      }
    end
  end


  def auth_callback
    if env['omniauth.error.type'] == :steamError
      redirect_to welcome_index_url, notice: 'Steam is down, please try again in sometime'
    else
      auth = request.env['omniauth.auth']
      session[:current_user] = {:nickname => auth.info['nickname'],
                                :image => auth.info['image'],
                                :uid => auth.uid}
      #todo: any chances of validation failure here ?
      current_user = User.find_by(steam_id: auth.uid)
      if !current_user
        current_user = User.create(steam_id: auth.uid, steam_image: auth.info['image'], steam_nickname: auth.info['nickname'], coaching_rate:0, mmr:0, is_a_coach:false)
        sync_steam_friends(auth, current_user)
      end
      redirect_to root_url
    end
  end

  def sync_steam_friends(auth, current_user)
    url = URI.parse(auth.info.urls.FriendList.to_s)
    res = Net::HTTP::get(url)
    friends = JSON.load(res)['friendslist']['friends']
    friends.each { |friend|
      friend_user = User.find_by(steam_id: friend['steamid'])
      if friend_user
        friend_relationship = FriendOf.new(from_node: current_user, to_node: friend_user, since: Time.now.to_i)
        friend_relationship.save
      end
    }
  end

  def login
  end

  def logout
    session[:current_user] = nil
    redirect_to root_url
  end

end
