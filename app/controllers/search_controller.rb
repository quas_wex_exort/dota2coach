class SearchController < ApplicationController

  def index
    @regions = Regions.all.map { |r| r.names }[0]
    chosen_region = params[:region]
    users = User.query_as(:u).where('u.is_a_coach = true and u.is_verified = true and u.mmr >= {min_mmr} and u.mmr <= {max_mmr} and u.coaching_rate >= {min_price} and u.coaching_rate <= {max_price}')
                .params(min_mmr: params[:mmr].scan(/\d+/)[0].to_i,
                        max_mmr: params[:mmr].scan(/\d+/)[1].to_i,
                        min_price: params[:amount].scan(/\d+/)[0].to_i,
                        max_price: params[:amount].scan(/\d+/)[1].to_i)
                .pluck(:u)

    filter_regions(chosen_region, users)
    filter_online_users

  end

  def filter_online_users
    if params['online']
      redis_client = Redis.new(:url => Rails.application.config.REDIS_URL)
      @users = @users.find_all do |user|
        redis_client.exists user.steam_id and redis_client.hgetall(user.steam_id).length > 0
      end
    end
  end

  def filter_regions(chosen_region, users)
    if 'Any'.casecmp(chosen_region.delete(' ')) != 0
      @users = users.find_all do |user|
        user.regions != nil and user.regions.include? chosen_region
      end
    else
      @users = users
    end
  end

end
