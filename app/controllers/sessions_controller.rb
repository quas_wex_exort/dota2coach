class OpenidController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def new

  end

  def create

  end

  def destroy

  end
  #
  # protected
  # def openid_consumer
  #   @openid_consumer ||= OpenID::Consumer.new(session, OpenID::FilesystemStore.new("#{RAILS_ROOT}/tmp/openid"))
  # end


end
