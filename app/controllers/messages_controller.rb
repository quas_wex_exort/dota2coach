class MessagesController < ApplicationController
  #todo: make sure you autheniticate logged in user

  def index
    current_user = User.find_by(steam_id: session[:current_user]['uid'])

    @users = []
    ordered_messages = current_user.latest_messages.sort{|m1,m2| m1.timestamp <=> m2.timestamp}.reverse!
    ordered_messages.each { |message|
      @users.push([message.from_user!= current_user ? message.from_user : message.to_user, (message.to_user==current_user and !message.is_read) ? false: true, Time.at(message.timestamp).to_formatted_s(:long_ordinal)])
    }
  end

  def update
    message = Message.find_by(uuid: params[:id])
    if message != nil
      message.is_read = true
      message.save
    end
    render :nothing => true
  end

  def create

    sender_id = session[:current_user]['uid']
    recipient_id = params[:recepient_id]

    from_user = User.find_by(steam_id: sender_id)
    to_user = User.find_by(steam_id: recipient_id)

    message = Message.create(body: params[:message][:body], timestamp: Time.now.to_i, is_read: false)

    latest_message = from_user.latest_messages(:lm).from_user.where(steam_id: recipient_id).pluck(:lm).any? ?
        from_user.latest_messages(:lm).from_user.where(steam_id: recipient_id).pluck(:lm) :
        from_user.latest_messages(:lm).to_user.where(steam_id: recipient_id).pluck(:lm)

    if latest_message.length > 0
      PreviousMessage.create(from_node: message, to_node: latest_message[0])
      from_user.latest_messages().first_rel_to(latest_message[0]).destroy
    end

    latest_message = to_user.latest_messages(:lm).from_user.where(steam_id: sender_id).pluck(:lm).any? ?
        to_user.latest_messages(:lm).from_user.where(steam_id: sender_id).pluck(:lm) :
        to_user.latest_messages(:lm).to_user.where(steam_id: sender_id).pluck(:lm)

    if latest_message.length > 0
      to_user.latest_messages().first_rel_to(latest_message[0]).destroy
    end


    MessageFrom.new(from_node: from_user, to_node: message).save
    MessageTo.new(from_node: message, to_node: to_user).save
    LatestMessage.new(from_node: from_user, to_node: message).save
    LatestMessage.new(from_node: to_user, to_node: message).save

    @message = message
    @receiver_id = recipient_id

    @conversation_id = (session[:current_user]['uid'] > params[:recepient_id] ? params[:recepient_id] + session[:current_user]['uid'] :
        session[:current_user]['uid'] + params[:recepient_id])
    # Create unique conversation id, lower + higher string

  end

end
