class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_new_messagers

  def set_new_messagers
    # if session[:current_user]
    #   current_user = User.find_by(steam_id: session['current_user']['uid'])
    #   @new_messager_ids = []
    #   current_user.latest_messages.each { |message|
    #     if message.to_user==current_user and !message.is_read
    #       @new_messager_ids.push message.from_user
    #     end
    #   }
    # end
  end

  protected
  def authenticate_user
    if session[:current_user]
      # set current user object to @current_user object variable
      @current_user = User.where(uid: session[:current_user]['uid']).first
      return true
    else
      redirect_to(:controller => 'welcome', :action => 'login')
      return false
    end
  end

end
